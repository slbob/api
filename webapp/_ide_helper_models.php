<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Rank
 *
 * @property int $id
 * @property string $name
 * @property int $access
 * @property bool $default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Staff[] $staff
 * @property-read int|null $staff_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Rank onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereAccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Rank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Rank withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Rank withoutTrashed()
 */
	class Rank extends \Eloquent {}
}

namespace App{
/**
 * App\AuditRecord
 *
 * @property int $id
 * @property string $added_by
 * @property string $prim_owner
 * @property string $prim_key
 * @property string $prim_sim
 * @property string $object_type
 * @property string $object_id
 * @property string $object_field
 * @property string $object_old_value
 * @property string $object_new_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereAddedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereObjectField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereObjectNewValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereObjectOldValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord wherePrimKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord wherePrimOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord wherePrimSim($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuditRecord whereUpdatedAt($value)
 */
	class AuditRecord extends \Eloquent {}
}

namespace App{
/**
 * App\Staff
 *
 * @property string $uuid
 * @property string $name
 * @property bool $active
 * @property int $rank_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Rank $rank
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Staff onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereRankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Staff withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Staff withoutTrashed()
 */
	class Staff extends \Eloquent {}
}

namespace App{
/**
 * App\ApiKey
 *
 * @property int $id
 * @property string $api_key
 * @property string $owner
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property bool $admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereApiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ApiKey whereUpdatedAt($value)
 */
	class ApiKey extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

