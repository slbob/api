<?php

namespace App\Http\Middleware;

use App\ApiKey;
use App\Exceptions\Unauthorized;
use Closure;

class AdminAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->header('auth');
        $this->apiKey = ApiKey::whereApiKey($key)->first();

        if (!$this->apiKey || !$this->apiKey->admin) {
            throw new Unauthorized();
        }
        
        return $next($request);
    }
}
