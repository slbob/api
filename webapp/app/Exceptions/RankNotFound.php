<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class RankNotFound extends Exception
{
    public function __construct($name, $code = 0, Throwable $previous = null) {
        $this->name = $name;
        parent::__construct($this->render(), $code, $previous); // construct the full context of the exception
    }

    public function render() {
        return response()->json("Rank $this->name can not be found!", 404);
    }
}
