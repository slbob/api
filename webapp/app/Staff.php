<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;


class Staff extends Model
{
    use SoftDeletes, Uuid;

    protected $revisionEnabled = true;
    protected $revisionCleanup = true;
    protected $historyLimit = 500;

    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $fillable = [
        'name',
        'uuid',
        'active',
        'rank_id',
    ];

    public function rank()
    {
        return $this->belongsTo(Rank::class);
    }
}
