<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rank extends Model
{
    use SoftDeletes;

    protected $revisionEnabled = true;
    protected $revisionCleanup = true;
    protected $historyLimit = 500;

    protected $fillable = [
        'name',
        'access',
    ];

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }
}
