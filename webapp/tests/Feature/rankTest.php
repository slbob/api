<?php

namespace Tests\Feature;

use App\ApiKey;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Str;

class rankTest extends TestCase
{
    use RefreshDatabase;

    private function generateApiKey(): string
    {
        $auth = new ApiKey;
        $auth->owner = "00000000-0000-0000-0000-000000000000";
        $auth->api_key = Str::random(40);
        $auth->save();

        return $auth->api_key;
    }

    /**
     * Test getting a list of Ranks
     *
     * @return void
     */
    public function testGetRankList(): void
    {
        $this->seed();

        $response = $this->get('/api/v1/ranks');

        $response->assertStatus(200);
    }
}
