<?php

namespace Tests\Feature;

use App\ApiKey;
use App\Staff;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;
use Str;

class staffTest extends TestCase
{
    use RefreshDatabase;

    private function generateApiKey(): string
    {
        $auth = new ApiKey;
        $auth->owner = "00000000-0000-0000-0000-000000000000";
        $auth->api_key = Str::random(40);
        $auth->save();

        return $auth->api_key;
    }

    /**
     * Test getting a specific Staff
     *
     * @return void
     */
    public function testGetValidStaff(): void
    {
        // setup a test user
        $this->seed();
        $staff = factory(Staff::class)->create();

        $staffUUID = $staff->uuid;
        $response = $this->get('/api/v1/staff/' . $staffUUID);

        $response->assertStatus(200)
            ->assertJson([
                'name' => $staff->name,
                'uuid' => $staff->uuid,
                'rank' => [
                    'name' => $staff->rank->name,
                    'access' => $staff->rank->access,
                ],
            ]);
    }

    /**
     * Test getting a list of Staff
     *
     * @return void
     */
    public function testGetStaffList(): void
    {
        $this->seed();

        $response = $this->get('/api/v1/staff');

        $response->assertStatus(200);
    }

    /**
     * Test getting a list of UUIDs
     *
     * @return void
     */
    public function testGetStaffUuidList(): void
    {
        $this->seed();

        $response = $this->get('/api/v1/staff/uuids');

        $response->assertStatus(200);
    }

    /**
     * Test creating a new Staff
     *
     * @return void
     */
    public function testCreateNewStaffMember(): void
    {
        $this->seed();

        $addStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => Str::random(),
            ],
        ];

        $response = $this->withHeaders([
            'Auth' => $this->generateApiKey(),
        ])->postJson('/api/v1/staff', $addStaffJson);


        $response->assertStatus(200)
            ->assertJson([
                'name' => $addStaffJson['staffMember']['name'],
                'uuid' => $addStaffJson['staffMember']['uuid'],
                'rank_id' => 1,
            ]);
    }

    /**
     * Test creating duplicate staff member
     *
     * @return void
     */
    public function testCreateDuplicateStaffMember(): void
    {
        $this->seed();
        $apiKey = $this->generateApiKey();

        $addStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => Str::random(),
            ],
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/staff', $addStaffJson);
        $response->assertStatus(200);

        // The staff should now exist, creating it again should throw an error

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/staff', $addStaffJson);
        $response->assertStatus(400)
            ->assertSeeText("Staff member with key {$addStaffJson['staffMember']['uuid']} already exists!");
    }

    /**
     * Test restore deleted staff member
     *
     * @return void
     */
    public function testRestoreDeletedStaffMember(): void
    {
        $this->seed();
        $apiKey = $this->generateApiKey();

        $addStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => Str::random(),
            ],
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/staff', $addStaffJson);
        $response->assertStatus(200);

        $response = $this->withHeaders([
            'Auth' => $apiKey
        ])->delete('/api/v1/staff/'.$addStaffJson['staffMember']['uuid']);
        $response->assertStatus(200)->assertSeeText(1);

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/staff', $addStaffJson);
        $response->assertStatus(200);
    }

    /**
     * Test updating a Staff member
     *
     * @return void
     */
    public function testUpdatingStaffMember(): void
    {
        $this->seed();

        $apiKey = $this->generateApiKey();

        // TODO: Do this better, if CreateStaffMember doesn't work, this won't work either.
        $addStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => Str::random(),
            ],
        ];

        $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/staff', $addStaffJson);

        $updateStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => $addStaffJson['staffMember']['uuid'],
                'rank' => 'Trainer',
                'name' => $addStaffJson['staffMember']['name'],
                'active' => true,
            ],
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->putJson('/api/v1/staff', $updateStaffJson);

        $response->assertStatus(200)
            ->assertJson([
                'name' => $updateStaffJson['staffMember']['name'],
                'uuid' => $updateStaffJson['staffMember']['uuid'],
                'rank_id' => 2,
            ]);
    }

    /**
     * Test missing Staff member
     *
     * @return void
     */
    public function testMissingStaffMember(): void
    {
        $this->seed();

        $apiKey = $this->generateApiKey();

        $updateStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => '00000000-0000-0000-0000-000000000000',
                'rank' => 'Trainer',
                'name' => 'Non Existant',
                'active' => true,
            ],
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->putJson('/api/v1/staff', $updateStaffJson);

        $response->assertStatus(404);
    }

    /**
     * Test Missing rank
     *
     * @return void
     */
    public function testUpdatingStaffMemberMissingRank(): void
    {
        $this->seed();

        $apiKey = $this->generateApiKey();

        // TODO: Do this better, if CreateStaffMember doesn't work, this won't work either.
        $addStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => Str::random(),
            ],
        ];

        $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/staff', $addStaffJson);

        $updateStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => $addStaffJson['staffMember']['uuid'],
                'rank' => 'Not a Rank!',
                'name' => $addStaffJson['staffMember']['name'],
                'active' => true,
            ],
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->putJson('/api/v1/staff', $updateStaffJson);

        $response->assertStatus(404);
    }

    /**
     * Test getting non-existent staff member
     *
     * @return void
     */
    public function testGettingNonExistentStaffMember(): void
    {
        $this->seed();

        $response = $this->get('/api/v1/staff/00000000-0000-0000-0000-000000000000');
        $response->assertStatus(404)
            ->assertSeeText('Staff member with key 00000000-0000-0000-0000-000000000000 can not be found!');
    }

    /**
     * Test getting invalid UUID
     *
     * @return void
     */
    public function testGettingInvalidUUID(): void
    {
        $this->seed();

        $response = $this->get('/api/v1/staff/0');
        $response->assertStatus(404)
            ->assertSeeText('Staff member with key 0 can not be found!');
    }
}
