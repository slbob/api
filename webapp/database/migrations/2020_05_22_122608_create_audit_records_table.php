<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_records', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('added_by');
            $table->uuid('prim_owner');
            $table->uuid('prim_key');
            $table->string('prim_sim', 255);

            $table->string('object_type'); // Eloquent model
            $table->string('object_id');

            $table->string('object_field');
            $table->string('object_old_value');
            $table->string('object_new_value');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_records');
    }
}
