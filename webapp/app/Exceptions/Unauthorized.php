<?php

namespace App\Exceptions;

use Exception;

class Unauthorized extends Exception
{
    /**
     * Render the exception as an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response | \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return response()->json('Unauthorized', 401);;
    }
}
