<?php

namespace Tests\Feature;

use App\ApiKey;
use App\Staff;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Str;

class apiKeyTest extends TestCase
{
    use RefreshDatabase;

    private function generateApiKey(bool $admin): string
    {
        $auth = new ApiKey;
        $auth->owner = "00000000-0000-0000-0000-000000000000";
        $auth->api_key = Str::random(40);
        $auth->admin = $admin;
        $auth->save();
        return $auth->api_key;
    }

    /**
     * Test unauthorized access without key
     *
     * @return void
     */
    public function testUnauthorized(): void
    {
        $this->seed();

        $response = $this->get('/api/v1/apikey');
        $response->assertStatus(401)->assertSeeText('Unauthorized');
    }

    /**
     * Test unauthorized with non-admin key
     *
     * @return void
     */
    public function testUnauthorizedKey(): void
    {
        $this->seed();
        $apiKey = $this->generateApiKey(false);

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->get('/api/v1/apikey');
        $response->assertStatus(401)->assertSeeText('Unauthorized');
    }


    /**
     * Test getting a list of API Keys
     *
     * @return void
     */
    public function testListKeys(): void
    {
        $this->seed();

        $apiKey = $this->generateApiKey(true);

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->get('/api/v1/apikey');

        $response->assertStatus(200);
    }

    /**
     * Test creating a new standard API Key
     *
     * @return void
     */
    public function testCreateStandardKey(): void
    {
        $this->seed();
        $apiKey = $this->generateApiKey(true);

        $staff = Staff::first();

        $postJson = [
            'owner' => $staff->uuid,
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/apikey', $postJson);
        $response->assertStatus(201);

        $this->assertDatabaseHas('api_keys', ['api_key' => $response->json('api_key')]);
    }

    /**
     * Test creating a new admin API Key
     *
     * @return void
     */
    public function testCreateAdminKey(): void
    {
        $this->seed();
        $apiKey = $this->generateApiKey(true);

        $staff = Staff::first();

        $postJson = [
            'owner' => $staff->uuid,
            'admin' => true,
        ];

        $response = $this->withHeaders([
            'Auth' => $apiKey,
        ])->postJson('/api/v1/apikey', $postJson);
        $response->assertStatus(201);

        $this->assertDatabaseHas('api_keys', ['api_key' => $response->json('api_key')]);
    }
}
