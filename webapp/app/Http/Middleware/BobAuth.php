<?php

namespace App\Http\Middleware;

use App\ApiKey;
use App\Exceptions\Unauthorized;
use Closure;

class BobAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->header('auth');

        $apiKey = ApiKey::whereApiKey($key)->first();

        if ($apiKey === null) {
            throw new Unauthorized();
        }

        return $next($request);
    }
}
