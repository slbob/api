<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class StaffAlreadyExists extends Exception
{
    public function __construct($uuid, $code = 0, Throwable $previous = null) {
        $this->uuid = $uuid;
        parent::__construct($this->render(), $code, $previous); // construct the full context of the exception
    }

    public function render() {
        return response()->json("Staff member with key $this->uuid already exists!", 400);
    }
}
