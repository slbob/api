<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->prefix('v1')->group(static function () {
    Route::get('/ranks', 'RankController@index');
    Route::get('/staff', 'StaffController@index');
    Route::get('/staff/uuids', 'StaffController@getUUIDs');
    Route::get('/staff/{uuid}', 'StaffController@show');
    Route::middleware('BobAuth')->group(static function () {
        Route::post('/staff', 'StaffController@store');
        Route::put('/staff', 'StaffController@update');
        Route::delete('/staff/{uuid}', 'StaffController@destroy');
    });

    Route::middleware('AdminAPI')->group(static function () {
        Route::get('/apikey', 'ApiKeyController@index');
        Route::post('/apikey', 'ApiKeyController@store');
    });
});



