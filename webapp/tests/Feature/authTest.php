<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Ramsey\Uuid\Uuid;

class authTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test failure to authenticate
     *
     * @return void
     */
    public function testFailureToAuthenticate(): void
    {
        $this->seed();
        
        $addStaffJson = [
            'addedBy' => '16568e82-20dc-4a6b-b2f7-3299921b0616',
            'primOwner' => 'e95043ca-b5d7-440a-be19-33089ac7d9e2',
            'primSim' => 'Tall Tails Estate',
            'primKey' => '70e12315-b3ac-44e0-67ad-7a61697c4c6b',
            'staffMember' => [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => "Something",
            ],
        ];

        $response = $this->postJson('/api/v1/staff', $addStaffJson);
        $response->assertStatus(401);
    }
}
