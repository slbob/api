<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid,
        'name' => $faker->name,
        'rank_id' => $faker->numberBetween(1,5),
        'active' => (bool)$faker->numberBetween(0,1),
    ];
});
