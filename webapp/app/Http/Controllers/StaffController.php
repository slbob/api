<?php

namespace App\Http\Controllers;

use App\AuditRecord;
use App\Exceptions\RankNotFound;
use App\Exceptions\StaffNotFound;
use App\Exceptions\StaffAlreadyExists;
use App\Staff;
use App\Rank;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use DB;

class StaffController extends Controller
{
    /**
     * Prepares basic query builder
     * @param bool $withInactive
     * @return Staff|\Illuminate\Database\Eloquent\Builder
     */
    private function getBasicQueryBuilder(bool $withInactive=false) {
        $queryBuilder = Staff::query();

        if (!$withInactive) {
            $queryBuilder = $queryBuilder->where('active', "=", true);
        }
        return $queryBuilder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $queryBuilder = $this->getBasicQueryBuilder($request->has('withInactive'));

        $queryBuilder = $queryBuilder->select([
            "uuid",
            "name",
            "active",
            "rank_id"
        ])->with(['Rank' => static function ($query) {
            $query->select('id', 'name', 'access');
        }]);

        return $queryBuilder->get();
    }

    public function getUUIDs(Request $request) {
        $queryBuilder = $this->getBasicQueryBuilder($request->has('withInactive'));
        $queryBuilder = $queryBuilder->select('uuid');
        // removing the parent object and returning a plain array out on http
        return $queryBuilder->get()->map(fn($item) => $item->uuid);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addStaffMember = $request->json()->all();

        $rank = Rank::where('default', '=', true)->first();

        $uuid = $addStaffMember['staffMember']['uuid'];
        $staff = Staff::where('uuid', '=', $uuid)->withTrashed()->first();
        if ($staff !== null && $staff->deleted_at === null) {
            throw new StaffAlreadyExists($uuid);
        }
        $staff = $staff ?? new Staff();
        if ($staff->trashed()) {
            $staff->restore();
        }
        $staff->uuid = $uuid;
        $staff->name = $addStaffMember['staffMember']['name'];
        $staff->rank_id = $rank->id;
        $staff->active = true;
        $staff->save();

        return response()->json($staff);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        if (!Uuid::isValid($uuid)) {
            throw new StaffNotFound($uuid);
        }
        $staff = Staff::where('uuid', '=', $uuid)->select([
            "uuid",
            "name",
            "active",
            "rank_id"
        ])->with(['Rank' => static function ($query) {
            $query->select('id', 'name', 'access');
        }])->first();
        if ($staff === null) {
            throw new StaffNotFound($uuid);
        }
        return response()->json($staff);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updateStaffMember = $request->json()->all();
        $staffMember = $updateStaffMember['staffMember'];

        $staff = Staff::where('uuid', '=', $staffMember['uuid'])->first();
        if ($staff === null) {
            throw new StaffNotFound($staffMember['uuid']);
        }
        $rank = Rank::where('name', '=', $staffMember['rank'])->first();
        if ($rank === null) {
            throw new RankNotFound($staffMember['rank']);
        }

        $staff->name = $staffMember['name'];
        $staff->active = $staffMember['active'];
        $staff->rank_id = $rank->id;
        $staff->save();

        return response()->json($staff);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $uuid)
    {
        $destroyedCount = Staff::destroy($uuid);
        return response()->json($destroyedCount);
    }
}
