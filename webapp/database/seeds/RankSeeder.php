<?php

use Illuminate\Database\Seeder;

class RankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainee = new \App\Rank();
        $trainee->default = true;
        $trainee->name = "Trainee";
        $trainee->access = 0b00000001;
        $trainee->save();

        $trainer = new \App\Rank();
        $trainer->name = "Trainer";
        $trainer->access = 0b00000011;
        $trainer->save();

        $stableMistress = new \App\Rank();
        $stableMistress->name = "Stable Mistress";
        $stableMistress->access = 0b01111111;
        $stableMistress->save();


        $stableMaster = new \App\Rank();
        $stableMaster->name = "Stable Master";
        $stableMaster->access = 0b01111111;
        $stableMaster->save();

        $stableOwner = new \App\Rank();
        $stableOwner->name = "Stable Owner";
        $stableOwner->access = 0b11111111;
        $stableOwner->save();
    }
}
