<?php

namespace App\Http\Controllers;

use App\ApiKey;
use App\Exceptions\Unauthorized;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Str;

class ApiKeyController extends Controller
{

    /**
     * List all API Keys and their owners
     *
     * @return ApiKey[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return ApiKey::all();
    }

    /**
     *
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $key = $request->all();
        $apiKey = new ApiKey($key);
        $apiKey->api_key = Str::random(40);
        $apiKey->save();
        return response($apiKey, 201);

    }

}
